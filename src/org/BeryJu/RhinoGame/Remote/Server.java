package org.BeryJu.RhinoGame.Remote;
import java.io.*;
import java.net.*;

public class Server{

	public String readFile(File f){
		BufferedReader br = null;
		String content = "";
		try {
			String line;
			br = new BufferedReader(new FileReader(f));
			while ((line = br.readLine()) != null) {
				content += line+"\n";
			}
			return content;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null){
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public Server(int port) throws Exception{
		String clientSentence;
		String capitalizedSentence;
		ServerSocket welcomeSocket = new ServerSocket(port);

		while(true){
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient =
				new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			clientSentence = inFromClient.readLine();
			if (clientSentence.equals("send")){
				outToClient.writeBytes(readFile(new File("RhinoGame.jar")));
				
			}
			System.out.println("Received: " + clientSentence);
			capitalizedSentence = clientSentence.toUpperCase() + '\n';
		}
	}
}