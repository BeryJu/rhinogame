package org.BeryJu.RhinoGame.RenderSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.BeryJu.RhinoGame.Utils;

public class SceneManager {

	private ArrayList<Scene> scenes = new ArrayList<Scene>();
	private Map<String, Integer> indices = new HashMap<String, Integer>();
	private int CurrentSceneIndex;
	private String CurrentSceneKey;
	private String LastSceneKey;
	
	public void addScene(Scene s, String k){
		Utils.log("Added scene "+s.getClass().getSimpleName()+" with Key \'"+k+"\'", this);
		this.scenes.add(s);
		this.indices.put(k, this.scenes.indexOf(s));
	}
	
	public void setScene(String k){
		this.LastSceneKey = this.CurrentSceneKey;
		try {
			this.CurrentSceneIndex = this.indices.get(k);
			this.CurrentSceneKey = k;
		}catch (NullPointerException e){
			Utils.log("Failed to change scene to \'" + k + "\': Scene not available or not implemented", this);
			System.exit(1);
		}
		Utils.log("Scene set to key \'"+k+"\'", this);
	}
	
	@Deprecated
	public void nextScene(){
		this.CurrentSceneIndex ++;
		Utils.log("Scene count increased, to "+this.CurrentSceneIndex, this);
	}
	
	public void previousScene(){
		String s = this.LastSceneKey;
		this.LastSceneKey = this.CurrentSceneKey;
		try {
			this.CurrentSceneIndex = this.indices.get(s);
			this.CurrentSceneKey = s;
		}catch (NullPointerException e){
			Utils.log("Failed to change scene to \'" + s + "\': Scene not available or not implemented", this);
			System.exit(1);
		}
		Utils.log("Scene rewarded to key \'"+s+"\'", this);
	}
	
	public Scene getCurrentScene(){
		return this.scenes.get(this.CurrentSceneIndex);
	}
	
}