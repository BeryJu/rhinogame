function Rhino(texture, X,Y, sound){

	this.STEP = 5;
	this._Texture;
	this._Position;
	this._Width = 0;
	this._Height = 0;
	this._Hitbox;
	this._Health = 100;
	this._Invisible;
	this._Hit;

	this.create = function(texture, X, Y, sound){
		this._Hit = sound;
		this._Position = new Vector2(X, Y);
		this._Hitbox = new Hitbox(this._Position.getX(), this._Position.getY(), 192, 96);
		this._Texture = texture;
		this._Width = new Display().getWidth();
		this._Height = new Display().getHeight();
	}

 	this.render = function(){
 		if (this._Health <= 10){
 			this.destroy();
		}
 		if (!(typeof this._Hitbox === "undefined")) {
	 		this._Hitbox = new Hitbox(this._Position.getX(), this._Position.getY(), 192, 96);
	 	}
 		this._Position.setX(this._Position.getX() - this.STEP);
  		if (this._Position.getX() < (0 - 192) ||
 			this._Position.getX() > (this.Width + 192)){
 			this.destroy();
 		}
		if (!this._Invisible){
			new GL().renderTexture(this._Texture, this._Position.getX(), this._Position.getY());
		}
 	}

 	this.destroy = function(){
 		this._Invisible = true;
 		this._Hitbox = undefined;
 	}

	this.create(texture, X,Y, sound);

}