importPackage(org.lwjgl.Input.Keyboard);
importPackage(org.lwjgl.Input.Mouse);

function InputAction(key, mode, callback){
	//Private inner class
	this.key = key;

	this.mode = mode;
	this.action = callback;
}

function Input(){
	
	this.Mouse = function(){
		return org.lwjgl.input.Mouse;
	}

	this.getMousePos = function(){
		var vec = new Vector2(0, 0);
		vec.setX(org.lwjgl.input.Mouse.getX());
		vec.setY(new Display().getHeight() - org.lwjgl.input.Mouse.getY());
		return vec;
	}

	this.InputActions = [];

	this.Keyboard = function(){
		return org.lwjgl.input.Keyboard;
	}

	this.bind = function(key, mode, action){
		Utils.log("Added binding to Input stack", this);
		this.InputActions.push(new InputAction(key, mode, action));
	}

	this.KEY_MODE_TAP = 0;
	this.KEY_MODE_RELEASE = 1;
	this.KEY_MODE_PRESS = 2;

	this.check = function(parent){
		foreach(this.InputActions, function(Action){
			if (org.lwjgl.input.Keyboard.isKeyDown(Action.key)) {
				if (Action.mode == 2){
					Action.action(parent);
				}
			}
		});
		while (org.lwjgl.input.Keyboard.next()) {
			if (org.lwjgl.input.Keyboard.getEventKeyState()) {
				foreach(this.InputActions, function(Action){
					if (Action.mode == 0){
						if (org.lwjgl.input.Keyboard.getEventKey() == Action.key){
							Action.action(parent);
						}
					}
				})
			}else{
				foreach(this.InputActions, function(Action){
					if (Action.mode == 1){
						if (org.lwjgl.input.Keyboard.getEventKey() == Action.key){
							Action.action(parent);
						}
					}
				})
			}
		}
	}

	//All the keycodes
	this.CHAR_NONE = 0;
	this.EVENT_SIZE = 18;
	this.KEY_0 = 11;
	this.KEY_1 = 2;
	this.KEY_2 = 3;
	this.KEY_3 = 4;
	this.KEY_4 = 5;
	this.KEY_5 = 6;
	this.KEY_6 = 7;
	this.KEY_7 = 8;
	this.KEY_8 = 9;
	this.KEY_9 = 10;
	this.KEY_A = 30;
	this.KEY_ADD = 78;
	this.KEY_APOSTROPHE = 40;
	this.KEY_APPS = 221;
	this.KEY_AT = 145;
	this.KEY_AX = 150;
	this.KEY_B = 48;
	this.KEY_BACK = 14;
	this.KEY_BACKSLASH = 43;
	this.KEY_C = 46;
	this.KEY_CAPITAL = 58;
	this.KEY_CIRCUMFLEX = 144;
	this.KEY_COLON = 146;
	this.KEY_COMMA = 51;
	this.KEY_CONVERT = 121;
	this.KEY_D = 32;
	this.KEY_DECIMAL = 83;
	this.KEY_DELETE = 211;
	this.KEY_DIVIDE = 181;
	this.KEY_DOWN = 208;
	this.KEY_E = 18;
	this.KEY_END = 207;
	this.KEY_EQUALS = 13;
	this.KEY_ESCAPE = 1;
	this.KEY_F = 33;
	this.KEY_F1 = 59;
	this.KEY_F10 = 68;
	this.KEY_F11 = 87;
	this.KEY_F12 = 88;
	this.KEY_F13 = 100;
	this.KEY_F14 = 101;
	this.KEY_F15 = 102;
	this.KEY_F2 = 60;
	this.KEY_F3 = 61;
	this.KEY_F4 = 62;
	this.KEY_F5 = 63;
	this.KEY_F6 = 64;
	this.KEY_F7 = 65;
	this.KEY_F8 = 66;
	this.KEY_F9 = 67;
	this.KEY_G = 34;
	this.KEY_GRAVE = 41;
	this.KEY_H = 35;
	this.KEY_HOME = 199;
	this.KEY_I = 23;
	this.KEY_INSERT = 210;
	this.KEY_J = 36;
	this.KEY_K = 37;
	this.KEY_KANA = 112;
	this.KEY_KANJI = 148;
	this.KEY_L = 38;
	this.KEY_LBRACKET = 26;
	this.KEY_LCONTROL = 29;
	this.KEY_LEFT = 203;
	this.KEY_LMENU = 56;
	this.KEY_LMETA = 219;
	this.KEY_LSHIFT = 42;
	this.KEY_LWIN = 219;
	this.KEY_M = 50;
	this.KEY_MINUS = 12;
	this.KEY_MULTIPLY = 55;
	this.KEY_N = 49;
	this.KEY_NEXT = 209;
	this.KEY_NOCONVERT = 123;
	this.KEY_NONE = 0;
	this.KEY_NUMLOCK = 69;
	this.KEY_NUMPAD0 = 82;
	this.KEY_NUMPAD1 = 79;
	this.KEY_NUMPAD2 = 80;
	this.KEY_NUMPAD3 = 81;
	this.KEY_NUMPAD4 = 75;
	this.KEY_NUMPAD5 = 76;
	this.KEY_NUMPAD6 = 77;
	this.KEY_NUMPAD7 = 71;
	this.KEY_NUMPAD8 = 72;
	this.KEY_NUMPAD9 = 73;
	this.KEY_NUMPADCOMMA = 179;
	this.KEY_NUMPADENTER = 156;
	this.KEY_NUMPADEQUALS = 141;
	this.KEY_O = 24;
	this.KEY_P = 25;
	this.KEY_PAUSE = 197;
	this.KEY_PERIOD = 52;
	this.KEY_POWER = 222;
	this.KEY_PRIOR = 201;
	this.KEY_Q = 16;
	this.KEY_R = 19;
	this.KEY_RBRACKET = 27;
	this.KEY_RCONTROL = 157;
	this.KEY_RETURN = 28;
	this.KEY_RIGHT = 205;
	this.KEY_RMENU = 184;
	this.KEY_RMETA = 220;
	this.KEY_RSHIFT = 54;
	this.KEY_RWIN = 220;
	this.KEY_S = 31;
	this.KEY_SCROLL = 70;
	this.KEY_SEMICOLON = 39;
	this.KEY_SLASH = 53;
	this.KEY_SLEEP = 223;
	this.KEY_SPACE = 57;
	this.KEY_STOP = 149;
	this.KEY_SUBTRACT = 74;
	this.KEY_SYSRQ = 183;
	this.KEY_T = 20;
	this.KEY_TAB = 15;
	this.KEY_U = 22;
	this.KEY_UNDERLINE = 147;
	this.KEY_UNLABELED = 151;
	this.KEY_UP = 200;
	this.KEY_V = 47;
	this.KEY_W = 17;
	this.KEY_X = 45;
	this.KEY_Y = 21;
	this.KEY_YEN = 125;
	this.KEY_Z = 44;
	this.KEYBOARD_SIZE = 25;

}