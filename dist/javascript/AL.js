importPackage(org.newdawn.slick.openal)
importPackage(org.newdawn.slick.util);

function Audio(){

	this._Data;
	
	this.sound = function(gain, pitch, loop){
		this._Data.playAsSoundEffect(gain, pitch, loop);
	}

	this.music = function(gain, pitch, loop){
		this._Data = playAsMusic(gain, pitch, loop);
	}

}

function AL(){
	
	this.put = function(){
		SoundStore.get().poll(0);
	}

	this.load = function(filename){
		Utils.log("Loaded Sound \'"+filename+"\'", this);
		var a = new Audio();
		a._Data = AudioLoader.getAudio("OGG", ResourceLoader.
			getResourceAsStream("assets/"+filename+".ogg"));
		return a;
	}

	this.destroy = function(){
		org.lwjgl.openal.AL.destroy();
	}

}