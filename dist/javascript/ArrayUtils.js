function foreach(array, callback, parent){
	if (typeof(parent) == "undefined"){
		for (var i = 0; i < array.length; i++) {
			callback(array[i]);
		}
	}else{
		for (var i = 0; i < array.length; i++) {
			callback(array[i], parent);
		}
	}
}