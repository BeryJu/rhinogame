function Player(name){

	this.isLeft;
	this.name;
	this.texture_l;
	this.texture_r;
	this.projectile;
	this.Projectiles = [];
	this.Position;
	this.Health = 100;
	this.Hitbox;
	this.Font;
	//this.JumpState { Up, Down, None }
	this.isJump;
	this.jumpState = "None";
	this.beginJumpY;
	this.jumpSpeed = 1.8;
	this.jumpHeight = 80.0;

	this.new = function(name, font){
		this.name = name;
		this.Font = font;
		this.Position = new Vector2(0, (new Display().getHeight() - 48));
		this.Hitbox = new Hitbox(this.Position.getX()+10, this.Position.getY(), 48, 27);
		this.texture_l = new GL().loadTexture("textures/Monkey_left");
		this.texture_r = new GL().loadTexture("textures/Monkey_right");
		this.projectile = new GL().loadTexture("textures/Projectile");
	}

	this.jump = function(){
		this.isJump = true;
		this.jumpState = "Up";
		this.beginJumpY = this.Position.getY();
	}

	this.checkJump = function(){
		if (this.isJump){
			if (this.jumpState == "Up"){
				this.Position.setY(this.Position.getY() + this.jumpHeight * this.jumpSpeed);
				if (this.Position.getY() >= this.beginJumpY){
					this.isJump = false;
					this.jumpState = "Down";
				}
			}else{
				this.Position.setY(this.Position.getY() - this.beginJumpY * this.jumpSpeed);
				if (this.Position.getY() <= this.beginJumpY){
					this.isJump = false;
					this.jumpState = "None";
					this.Position.setY(0);
				}
			}
		}
	}

	this.render = function(){
		this.Hitbox = new Hitbox(this.Position.getX()+10, this.Position.getY(), 48, 27);
		if (this.isLeft){
			new GL().renderTexture(this.texture_l, this.Position.getX(), this.Position.getY());
		}else{
			new GL().renderTexture(this.texture_r, this.Position.getX(), this.Position.getY());
		}
	}

	this.shoot = function(){
		this.Projectiles.push(new Projectile(this.projectile, this.Position, new Input().getMousePos())); 
	}

	this.new(name);

}