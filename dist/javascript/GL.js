importPackage(org.lwjgl);
importPackage(org.lwjgl.opengl);
importPackage(org.BeryJu.RhinoGame);
//For screenshots
importPackage(java.io);
importPackage(java.awt.image);
importPackage(java.nio);
importPackage(javax.imageio);
//For texture
importClass(java.awt.Font);
importPackage(org.newdawn.slick);
importClass(org.newdawn.slick.util.ResourceLoader);
importPackage(org.newdawn.slick.opengl);
function GL(){

	this.takeScreenshot = function(){
		GL11.glReadBuffer(GL11.GL_FRONT);
		var width = new Display().getWidth();
		var height= new Display().getHeight();
		var bpp = 4; // Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
		var buffer = BufferUtils.createByteBuffer(width * height * bpp);
		GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );
		var file = new File(System.currentTimeMillis()+".png");
		var format = "PNG";
		var image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for(var x = 0; x < width; x++){
			for(var y = 0; y < height; y++){
				var i = (x + (width * y)) * bpp;
				var r = buffer.get(i) & 0xFF;
				var g = buffer.get(i + 1) & 0xFF;
				var b = buffer.get(i + 2) & 0xFF;
				image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
			}
		}
		try {
			ImageIO.write(image, format, file);
		} catch (error) {
			Utils.log("Error during File I/O: "+error, this);
		}
		Utils.log("Saved Screenshot "+file.getName(), this);
	}

	this.loadTexture = function(key){
		if (new File("assets/"+key+".tga").exists() == false) {
			Utils.log("Texture \'"+key+"\' not found", this);
		} else {
			Utils.log("Loaded Texture \'"+key+"\'", this);
			return TextureLoader.getTexture("TGA",
					ResourceLoader.getResourceAsStream("assets/"+key+".tga"));
		}
	}

	this.initOpenGL = function(w, h){
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, w, h, 0, 0, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}

	this.render = function(){
		org.lwjgl.opengl.Display.sync(60);
		org.lwjgl.opengl.Display.update();
		this.FPS.update();
		this.FPS.frameTime();
	}

	this.clearCanvas = function(){
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); 
	}

	this.renderTexture = function(texture, x, y, r){
		x = parseInt(x);
		y = parseInt(y);
		r = parseInt(r);
		if (typeof(x) === "number" && typeof(y) === "number"){
			try{
				texture.bind();
				GL11.glColor3f(1, 1, 1);
				GL11.glRotatef(r, 0, 0, 1);
				GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex2f(x, y);
				GL11.glTexCoord2f(texture.getWidth(), 0);
				GL11.glVertex2f(x + texture.getImageWidth(), y);
				GL11.glTexCoord2f(texture.getWidth(), texture.getHeight());
				GL11.glVertex2f(x + texture.getImageWidth(), y + texture.getImageHeight());
				GL11.glTexCoord2f(0, texture.getHeight());
				GL11.glVertex2f(x, y + texture.getImageHeight());
				GL11.glEnd();
			}catch (err){
				// Utils.log(x.toString()+", "+y.toString(), this);
				// Utils.log(err, this);
			}
		}else{
			Utils.log("X or Y is NaN", this);
		}
	}

	this.renderTexture = function(texture, x, y) {
		x = parseInt(x);
		y = parseInt(y);
		if (typeof(x) === "number" && typeof(y) === "number"){
			try{
				texture.bind();
				GL11.glColor3f(1, 1, 1);
				GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex2f(x, y);
				GL11.glTexCoord2f(texture.getWidth(), 0);
				GL11.glVertex2f(x + texture.getImageWidth(), y);
				GL11.glTexCoord2f(texture.getWidth(), texture.getHeight());
				GL11.glVertex2f(x + texture.getImageWidth(), y + texture.getImageHeight());
				GL11.glTexCoord2f(0, texture.getHeight());
				GL11.glVertex2f(x, y + texture.getImageHeight());
				GL11.glEnd();
			}catch (err){
				// Utils.log(x.toString()+", "+y.toString(), this);
				// Utils.log(err, this);
			}
		}else{
			Utils.log("X or Y is NaN", this);
		}
	}

	this.loadFont = function(name, size){
		if (new File("./assets/"+name+".ttf").exists()) {
			try{
				var inputStream = ResourceLoader.getResourceAsStream("./assets/"+name+".ttf");
				var awtFont2 = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, inputStream);
				awtFont2 = awtFont2.deriveFont(size); // set font size
				return TrueTypeFont(awtFont2, true);
			}catch (e){
				Utils.log(e, this);
			}
		} else {
			Utils.log("Font "+name+" could not be loaded", null);
		}
	}

	this.FPS = new FPS();
	
}

function FPS(){
	this.lastFrameTime = 0;
	this.currentFrameTime = 0;

	this.frameTime = function(){
		var d = this.getTime();
		this.currentFrameTime = d - this.lastFrameTime;
		this.lastFrameTime = d;
	}

	this.lastFrame = 0;
	this.fps = 0;
	this.lastFPS = 0;
	this.actualFPS = 0;

	this.create = function() {
		this.getDelta();
		this.lastFPS = this.getTime();
	}
	
	this.getDelta = function() {
		var time = this.getTime();
		var delta = (time - this.lastFrame);
		this.lastFrame = time;
		return delta;
	}

	this.getTime = function() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	this.getFPS = function(){
		return this.actualFPS;
	}

	this.update = function() {
		this.getDelta();
		if (this.getTime() - this.lastFPS > 1000) {
			this.actualFPS = this.fps;
			this.fps = 0;
			this.lastFPS += 1000;
		}
		this.fps++;
	}

	this.create();
}