/**
 * @author BeryJu, ported from CodingUniverse
 */
importPackage(org.lwjgl);
importPackage(org.lwjgl.util.vector);
importPackage(java.io);
importPackage(java.nio);
function ModelLoader(){
	this.createDisplayList = function(model) {
		var displayList = GL11.glGenLists(1);
		GL11.glNewList(displayList, GL11.GL_COMPILE);
		GL11.glBegin(GL11.GL_TRIANGLES);
		foreach(model.faces, function(e){
			var n1 = model.normals[parseInt(e.normal.x - 1)];
			GL11.glNormal3f(n1.x, n1.y, n1.z);
			var v1 = model.vertices[parseInt(e.vertex.x - 1)];
			GL11.glVertex3f(v1.x, v1.y, v1.z);
			var n2 = model.normals[parseInt(e.normal.y - 1)];
			GL11.glNormal3f(n2.x, n2.y, n2.z);
			var v2 = model.vertices[parseInt(e.vertex.y - 1)];
			GL11.glVertex3f(v2.x, v2.y, v2.z);
			var n3 = model.normals[parseInt(e.normal.z - 1)];
			GL11.glNormal3f(n3.x, n3.y, n3.z);
			var v3 = model.vertices[parseInt(e.vertex.z - 1)];
			GL11.glVertex3f(v3.x, v3.y, v3.z);
		});
		GL11.glEnd();
		GL11.glEndList();
		return displayList;
	}

	this.reserveData = function(size) {
		return BufferUtils.createFloatBuffer(size);
	}

	this.asFloats = function(v) {
		var arr = [v.x, v.y, v.z];
		return arr;
	}

	this.createVBO = function(model) {
		var vboVertexHandle = GL11.glGenBuffers();
		var vboNormalHandle = GL11.glGenBuffers();
		var vertices = reserveData(model.faces.size() * 9);
		var normals = reserveData(model.faces.size() * 9);
		foreach(model.faces, function(e){
			vertices.push(asFloats(model.vertices[parseInt(e.vertex.x - 1)]));
			vertices.push(asFloats(model.vertices[parseInt(e.vertex.y - 1)]));
			vertices.push(asFloats(model.vertices[parseInt(e.vertex.z - 1)]));
			normals.push(asFloats(model.normals[parseInt(e.normal.x - 1)]));
			normals.push(asFloats(model.normals[parseInt(e.normal.y - 1)]));
			normals.push(asFloats(model.normals[parseInt(e.normal.z - 1)]));
		});
		vertices.flip();
		normals.flip();
		GL11.glBindBuffer(GL_ARRAY_BUFFER, vboVertexHandle);
		GL11.glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);
		GL11.glVertexPointer(3, GL_FLOAT, 0, 0);
		GL11.glBindBuffer(GL_ARRAY_BUFFER, vboNormalHandle);
		GL11.glBufferData(GL_ARRAY_BUFFER, normals, GL_STATIC_DRAW);
		GL11.glNormalPointer(GL_FLOAT, 0, 0);
		GL11.glBindBuffer(GL_ARRAY_BUFFER, 0);
		return [vboVertexHandle, vboNormalHandle];
	}

	this.loadSTL = function(f){
		var reader = new BufferedReader(new FileReader("assets/model/"+f+".obj"));
		var model = new Model();
		var line;
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("v ")) {
				var x = parseFloat(line.split(" ")[1]);
				var y = parseFloat(line.split(" ")[2]);
				var z = parseFloat(line.split(" ")[3]);
				model.vertices.push(new Vector3f(x, y, z));
			} else if (line.startsWith("facet normal")) {
				var x = parseFloat(line.split(" ")[1]);
				var y = parseFloat(line.split(" ")[2]);
				var z = parseFloat(line.split(" ")[3]);
				model.normals.push(new Vector3f(x, y, z));
			} else if (line.startsWith("vt ")) {
				
			} else if (line.startsWith("f ")) {
				var vertexIndices = new Vector3f(parseFloat(line.split(" ")[1].split("/")[0]),
						parseFloat(line.split(" ")[2].split("/")[0]),
						parseFloat(line.split(" ")[3].split("/")[0]));
				var normalIndices = new Vector3f(parseFloat(line.split(" ")[1].split("/")[2]),
						parseFloat(line.split(" ")[2].split("/")[2]),
						parseFloat(line.split(" ")[3].split("/")[2]));
				model.faces.push(new Face(vertexIndices, normalIndices));
			}
		}
		reader.close();
		Utils.log("Loaded STL Model \'"+f+"\'", this);
		return model;
	}

	this.loadOBJ = function(f){
		var reader = new BufferedReader(new FileReader("assets/model/"+f+".obj"));
		var model = new Model();
		var line;
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("v ")) {
				var x = parseFloat(line.split(" ")[1]);
				var y = parseFloat(line.split(" ")[2]);
				var z = parseFloat(line.split(" ")[3]);
				model.vertices.push(new Vector3f(x, y, z));
			} else if (line.startsWith("vn ")) {
				var x = parseFloat(line.split(" ")[1]);
				var y = parseFloat(line.split(" ")[2]);
				var z = parseFloat(line.split(" ")[3]);
				model.normals.push(new Vector3f(x, y, z));
			} else if (line.startsWith("vt ")) {
				
			} else if (line.startsWith("f ")) {
				var vertexIndices = new Vector3f(parseFloat(line.split(" ")[1].split("/")[0]),
						parseFloat(line.split(" ")[2].split("/")[0]),
						parseFloat(line.split(" ")[3].split("/")[0]));
				var normalIndices = new Vector3f(parseFloat(line.split(" ")[1].split("/")[2]),
						parseFloat(line.split(" ")[2].split("/")[2]),
						parseFloat(line.split(" ")[3].split("/")[2]));
				model.faces.push(new Face(vertexIndices, normalIndices));
			}
		}
		reader.close();
		Utils.log("Loaded OBJ Model \'"+f+"\'", this);
		return model;
	}
}