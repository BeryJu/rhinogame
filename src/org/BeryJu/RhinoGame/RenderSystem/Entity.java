package org.BeryJu.RhinoGame.RenderSystem;

import java.util.ArrayList;

import org.BeryJu.RhinoGame.Main;
import org.newdawn.slick.opengl.Texture;

public abstract class Entity {

	protected ArrayList<Entity> entities = new ArrayList<Entity>();
	protected int Width;
	protected int Height;
	protected Texture Img;
	protected String TextureKey;
	protected int X;
	protected int Y;
	protected EntityHandler Handler;
	protected String Text;

	public String getTextureKey(){
		return this.TextureKey;
	}
	
	public void setTextureKey(String NewTextureKey){
		this.TextureKey = NewTextureKey;
	}
	
	
	public Texture getTexture(){
		return this.Img;
	}
	
	public void setTexture(Texture NewTexture){
		this.Img = NewTexture;
	}
	
	public String getText() {
		return this.Text;
	}

	public ArrayList<Entity> getEntities() {
		return this.entities;
	}

	public void setText(String NewText) {
		this.Text = NewText;
	}

	public void addSubEntity(Entity e) {
		this.entities.add(e);
	}

	public void setHandler(EntityHandler h) {
		this.Handler = h;
	}

	public void render(float Delta, Main instance) {
		for (Entity e : this.entities) {
			e.render(Delta, instance);
		}
	}

	public int getWidth() {
		return Width;
	}

	public void setWidth(int width) {
		Width = width;
	}

	public int getHeight() {
		return Height;
	}

	public void setHeight(int height) {
		Height = height;
	}

	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}
}