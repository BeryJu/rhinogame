importClass(org.lwjgl.util.Rectangle);
function Hitbox(X, Y, Width, Height){

	this.X = X;
	this.Y = Y;
	this.Width = Width;
	this.Height = Height;

	this.intersects = function(hitbox){
		if (typeof this === "undefined" ||
			typeof hitbox === "undefined"){
			return false;
		}else{
			var a = new Rectangle(this.X, this.Y, this.Width, this.Height);
			var b = new Rectangle(hitbox.X, hitbox.Y,hitbox.Width, hitbox.Height);
			return a.intersects(b);
		}
	}

}