function Projectile(texture,from, to){

	this.texture;	
	this.position;
	this.Destination;
	this.Speed = new Vector2(5,5);
	this.Width = 0;
	this.Hitbox;
	this.invisible;

	this.new = function(texture,from, to){
		this.position = from;
		this.Destination = to;
 		if (!(typeof this.Hitbox === "undefined")) {
	 		this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 	}
		this.texture = texture;
		this.Width = new Display().getWidth();
	}

 	this.destroy = function(){
 		this.invisible = true;
 		this.texture = undefined
 		this.Hitbox = undefined;
 	}

 	this.isOffScreen = function(){
 		var width = new Display().getWidth();
 		var height = new Display().getHeigth();
 		if (this.position.getX() < 0-48 ||
 			this.position.getX() > width+48){
 			return true;
 		}
 		if (this.position.getY() < 0-48 ||
 			this.position.getY() > height+48){
			return true;
		}
		return false;
 	}

 	this.render = function(){
 		try{
	 		if (this.isOffScreen){
	 			//this.destroy();
	 		}
			this.position = this.position.VectorInterpolate(this.position, this.Destination, this.Speed);
	 		//new Display().setTitle(this.position.toString());
			this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 		if (!this.invisible){
		 		new GL().renderTexture(this.texture, this.position.getX(), this.position.getY());
	 		}
 		}catch(err){
 			Utils.log(err, this);
 		}
 	}

	this.new(texture,from, to);

}