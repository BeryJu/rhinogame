function Player(name, font){

	this._IsLeft;
	this._Name;
	this._TextureLeft;
	this._TextureRight;
	this._TextureProjectile;
	this._Projectiles = [];
	this._Position;
	this._Health = 100;
	this._Hitbox;
	this._Font;
	this._Score = 0;
	// this.JumpState { Up, Down, None }
	// this.isJump;
	// this.jumpState = "None";
	// this.beginJumpY;
	// this.jumpSpeed = 1.8;
	// this.jumpHeight = 80.0;

	this.create = function(name, font){
		this._Name = name;
		this._Font = font;
		this._Position = new Vector2(0, (new Display().getHeight() - (50 + font.getHeight())));
		this._Hitbox = new Hitbox(this._Position.getX()+10, this._Position.getY(), 48, 27);
		this._TextureLeft = new GL().loadTexture("textures/Monkey_left");
		this._TextureRight = new GL().loadTexture("textures/Monkey_right");
		this._TextureProjectile = new GL().loadTexture("textures/Projectile");
	}

	this.getHitbox = function(){
		return this._Hitbox;
	}

	// this.jump = function(){
	// 	this.isJump = true;
	// 	this.jumpState = "Up";
	// 	this.beginJumpY = this._Position.getY();
	// }
	//
	// this.checkJump = function(){
	// 	if (this.isJump){
	// 		if (this.jumpState == "Up"){
	// 			this._Position.setY(this._Position.getY() + this.jumpHeight * this.jumpSpeed);
	// 			if (this._Position.getY() >= this.beginJumpY){
	// 				this.isJump = false;
	// 				this.jumpState = "Down";
	// 			}
	// 		}else{
	// 			this._Position.setY(this._Position.getY() - this.beginJumpY * this.jumpSpeed);
	// 			if (this._Position.getY() <= this.beginJumpY){
	// 				this.isJump = false;
	// 				this.jumpState = "None";
	// 				this._Position.setY(0);
	// 			}
	// 		}
	// 	}
	// }

	this.render = function(){
		this._Hitbox = new Hitbox(this._Position.getX()+10, this._Position.getY(), 48, 27);
		this._Font.drawString(this._Position.getX(), this._Position.getY(), this._Name);
		if (this._IsLeft){
			new GL().renderTexture(this._TextureLeft, this._Position.getX(),
				 this._Position.getY() + this._Font.getHeight(), 1);
		}else{
			new GL().renderTexture(this._TextureRight, this._Position.getX(),
				 this._Position.getY() + this._Font.getHeight(), 1);
		}
	}

	this.shoot = function(){
		this._Projectiles.push(new Projectile(this._TextureProjectile,
			this._Position, new Input().getMousePos())); 
	}

	this.create(name, font);

}