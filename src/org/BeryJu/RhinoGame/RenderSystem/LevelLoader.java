/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.BeryJu.RhinoGame.RenderSystem;

import flexjson.*;
import java.io.*;
import org.BeryJu.RhinoGame.Level;
import org.BeryJu.RhinoGame.Utils;

/**
 *
 * @author BeryJu
 */
public class LevelLoader {
	
	public static Level load(String path) throws Exception{
		// Reader r = new FileReader(path);
		// Level l = new JSONDeserializer<Level>().deserialize(r);
		// for (Entity e : l.entities){
		// 	e.setTexture(Utils.loadTexture(e.getTextureKey(), null));
		// }
		// return l;
		return null;
	}
	
	public static void save(Level l, String path) throws IOException{
		Writer os = new FileWriter(path);
		String JSON = new JSONSerializer().deepSerialize(l);
		os.write(JSON);
		os.close();
	}
	
}
