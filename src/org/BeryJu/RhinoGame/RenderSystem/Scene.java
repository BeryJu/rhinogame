package org.BeryJu.RhinoGame.RenderSystem;

import java.util.ArrayList;

import org.BeryJu.RhinoGame.Main;

public class Scene {

	private ArrayList<Entity> entities = new ArrayList<Entity>();
	
	public Scene addEntity(Entity e){
		this.entities.add(e);
		return this;
	}
	
	public ArrayList<Entity> getEntites(){
		return this.entities;
	}
	
	public void render(float Delta, Main instance){
		for (Entity e : entities){
			e.render(Delta, instance);
		}
	}
	
}