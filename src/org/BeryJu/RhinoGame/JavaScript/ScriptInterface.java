package org.BeryJu.RhinoGame.JavaScript;
import java.io.*;
import java.lang.reflect.*;
import javax.script.*;
import org.BeryJu.RhinoGame.Main;
import org.BeryJu.RhinoGame.Utils;
import org.BeryJu.RhinoGame.JavaScript.PreProcessor;
import org.BeryJu.RhinoGame.JavaScript.PreProcessor.LibLoader;
import org.BeryJu.RhinoGame.JavaScript.PreProcessor.Annotation;
/**
* Interface to evaluate Scripts
* @author BeryJu
*/
public class ScriptInterface {

	public File script = null;
	private ScriptEngine engine = null;
	private Main instance;
	private LibLoader pre;
	private String[] args;

	public ScriptInterface(File f, Main instance, String[] args) throws ScriptException, FileNotFoundException{
		if (f.exists()){
			this.script = f;
		}else{
			throw new FileNotFoundException();
		}
		ScriptEngineManager manager = new ScriptEngineManager();
		this.engine = manager.getEngineByName("JavaScript");
		this.instance = instance;
		this.pre = new PreProcessor().new LibLoader("javascript/");
		this.args = args;
	}

	public ScriptInterface(String f, Main instance, String[] args) throws ScriptException, FileNotFoundException{
		File tf = new File(f);
		if (tf.exists()){
			this.script = tf;
		}else{
			throw new FileNotFoundException();
		}
		ScriptEngineManager manager = new ScriptEngineManager();
		this.engine = manager.getEngineByName("JavaScript");
		this.instance = instance;
		this.pre = new PreProcessor().new LibLoader("javascript/");
		this.args = args;
	}

	private void dump(String c, String name){
		if (new File(name).exists()){
			new File(name).delete();
		}
		try{
			BufferedWriter out = new BufferedWriter(new FileWriter(name));
			out.write(c);
			out.close();
		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}

	public void evaluate(){
		engine.put("Rhino_Instance", this.instance);
		engine.put("Rhino_Arguments", this.args);
		new File("dump/").mkdir();
		String lib = this.pre.append();
		try{
			engine.eval(lib);
		}catch (Exception e){
			Utils.log("Error RhinoGame library script evaluation.", this);
			Utils.log("Source has been dumped to dump.js", this);
			this.dump(lib, "dump/dump.js");
			e.printStackTrace();
			System.exit(1);			
		}
		String sc = readFile(this.script);
		try{
			if (Main.dump){
				this.dump(sc, "dump/g.js");
				this.dump(lib, "dump/lib.js");
			}
			engine.eval(sc);
			Utils.log("Startup Time: "+
				String.valueOf(System.currentTimeMillis() - Main.StartTime)
				+"ms", this);
			Invocable inv = (Invocable) this.engine;
			inv.invokeFunction("main");
		}catch (Exception e){
			Utils.log("Error during game script evaluation.", this);
			this.dump(sc, "dump/g.js");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public String readFile(File f){
		BufferedReader br = null;
		String content = "";
		Annotation a = new PreProcessor().new Annotation();
		try {
			String line;
			br = new BufferedReader(new FileReader(f));
			while ((line = br.readLine()) != null) {
				if (line.startsWith("//") == false){
					content += a.line(line, this.pre)+"\n";
				}
			}
			return content;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null){
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

}