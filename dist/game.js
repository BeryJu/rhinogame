importPackage(flexjson);
importPackage(org.BeryJu.RhinoGame);
importPackage(org.BeryJu.RhinoGame.RenderSystem);
@load<javascript_game/>
var Instance = Rhino_Instance;
var Arguments = Rhino_Arguments;
function main(){
	inst = new RhinoGame(Instance, Arguments);
	try{
		inst.run();
	}catch (err){
		stackTrace(err);
	}
}

function RhinoGame(Instance, Arguments){

	this._Instance = Instance;
	this._Arguments = Arguments;
	this.camera;
	this._Player;
	this.bunnyDisplayList;
	this._ModelLoader;
	this._Font;

	this.run = function() {

		this._ModelLoader = new ModelLoader();
		this.setUpDisplay();
		this.setUpDisplayLists();
		Utils.log("Model loaded", this);
		var aspect = parseFloat(org.lwjgl.opengl.Display.getWidth() /
					 org.lwjgl.opengl.Display.getHeight());
		var derp = new Builder();
		Utils.log("Camera setup'd", this);
		derp.setAspectRatio(aspect)
		derp.setRotation(-1.12, 0.16, 0)
		derp.setPosition(-1.38, 1.36, 7.95)
		this.camera = derp.build();
		this.camera.applyOptimalStates();
		this.camera.applyPerspectiveMatrix();

		// this._Player = new Player("BeryJu", this._Font);
		// this._Font = new GL().loadFont("ubuntu", 28);
		while (!org.lwjgl.opengl.Display.isCloseRequested()) {
			this.render();
			this.checkInput();
			org.lwjgl.opengl.Display.update();
			org.lwjgl.opengl.Display.sync(60);
		}
		this.cleanUp();
	}

	this.setUpDisplayLists = function(){
		var bunny = this._ModelLoader.loadOBJ("Bullet");
		this.bunnyDisplayList = this._ModelLoader.createDisplayList(bunny);
	}

	this.checkInput = function() {
		this.camera.processMouse(1, 80, -80);
		//delta, speed
		this.camera.processKeyboard(16, 1);
		if (Mouse.isButtonDown(0))
			Mouse.setGrabbed(true);
		else if (Mouse.isButtonDown(1))
			Mouse.setGrabbed(false);
	}

	this.cleanUp = function() {
		GL11.glDeleteLists(this.bunnyDisplayList, 1);
		org.lwjgl.opengl.Display.destroy();
	}

	this.render = function() {
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		/*this._Font.drawString(0,30,this._Instance.getMemoryPercentage().toString()
			+"% Memory used",  Color.white);*/
		GL11.glLoadIdentity();
		this.camera.applyTranslations();
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		// this._Player.render();
		// this._Font.drawString(0,0, "RhinoGame Engine build"+this._Instance.Build);
		GL11.glCallList(this.bunnyDisplayList);
	}

	this.setUpDisplay = function() {
		try {
			org.lwjgl.opengl.Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(1280, 720));
			org.lwjgl.opengl.Display.setVSyncEnabled(true);
			org.lwjgl.opengl.Display.setTitle("RhinoGame");
			org.lwjgl.opengl.Display.create();
		} catch (err) {
			System.err.println("The display wasn't initialized correctly. :(");
			org.lwjgl.opengl.Display.destroy();
			System.exit(1);
		}
	}

}

