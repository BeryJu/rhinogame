importPackage(flexjson);
importPackage(org.BeryJu.RhinoGame);
importPackage(org.BeryJu.RhinoGame.RenderSystem);
//Projectile.js
function Projectile(texture,from, to){

	this.texture;	
	this.position;
	this.Direction;
	this.Speed = new Vector2(5,5);
	this.Width = 0;
	this.Hitbox;
	this.invisible;

	this.create = function(texture,from, to){
		this.position = from;
		this.Direction = VectorNormalized(VectorSub(to, this.position));
 		if (!(typeof this.Hitbox === "undefined")) {
	 		this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 	}
		this.texture = texture;
		this.Width = new Display().getWidth();
	}

 	this.destroy = function(){
 		this.invisible = true;
 		this.texture = undefined
 		this.Hitbox = undefined;
 	}

 	this.isOffScreen = function(){
 		var width = new Display().getWidth();
 		var height = new Display().getHeight();
 		if (this.position.getX() < 0-48 ||
 			this.position.getX() > width+48){
 			return true;
 		}
 		if (this.position.getY() < 0-48 ||
 			this.position.getY() > height+48){
			return true;
		}
		return false;
 	}

 	this.render = function(){
 		try{
	 		if (this.isOffScreen() == true){
	 			this.destroy();
	 		}
			this.position = VectorAdd(this.position, VectorMul(this.Direction, this.Speed));
	 		//new Display().setTitle(this.position.toString());
			this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 		if (!this.invisible){
		 		new GL().renderTexture(this.texture, this.position.getX(), this.position.getY());
	 		}
 		}catch(err){
 			Utils.log(err, this);
 		}
 	}

	this.create(texture,from, to);

}

//derp.js

//Player.js
function Player(name, font){

	this._IsLeft;
	this._Name;
	this._TextureLeft;
	this._TextureRight;
	this._TextureProjectile;
	this._Projectiles = [];
	this._Position;
	this._Health = 100;
	this._Hitbox;
	this._Font;
	this._Score = 0;
	// this.JumpState { Up, Down, None }
	// this.isJump;
	// this.jumpState = "None";
	// this.beginJumpY;
	// this.jumpSpeed = 1.8;
	// this.jumpHeight = 80.0;

	this.create = function(name, font){
		this._Name = name;
		this._Font = font;
		this._Position = new Vector2(0, (new Display().getHeight() - (50 + font.getHeight())));
		this._Hitbox = new Hitbox(this._Position.getX()+10, this._Position.getY(), 48, 27);
		this._TextureLeft = new GL().loadTexture("textures/Monkey_left");
		this._TextureRight = new GL().loadTexture("textures/Monkey_right");
		this._TextureProjectile = new GL().loadTexture("textures/Projectile");
	}

	this.getHitbox = function(){
		return this._Hitbox;
	}

	// this.jump = function(){
	// 	this.isJump = true;
	// 	this.jumpState = "Up";
	// 	this.beginJumpY = this._Position.getY();
	// }
	//
	// this.checkJump = function(){
	// 	if (this.isJump){
	// 		if (this.jumpState == "Up"){
	// 			this._Position.setY(this._Position.getY() + this.jumpHeight * this.jumpSpeed);
	// 			if (this._Position.getY() >= this.beginJumpY){
	// 				this.isJump = false;
	// 				this.jumpState = "Down";
	// 			}
	// 		}else{
	// 			this._Position.setY(this._Position.getY() - this.beginJumpY * this.jumpSpeed);
	// 			if (this._Position.getY() <= this.beginJumpY){
	// 				this.isJump = false;
	// 				this.jumpState = "None";
	// 				this._Position.setY(0);
	// 			}
	// 		}
	// 	}
	// }

	this.render = function(){
		this._Hitbox = new Hitbox(this._Position.getX()+10, this._Position.getY(), 48, 27);
		this._Font.drawString(this._Position.getX(), this._Position.getY(), this._Name);
		if (this._IsLeft){
			new GL().renderTexture(this._TextureLeft, this._Position.getX(),
				 this._Position.getY() + this._Font.getHeight(), 1);
		}else{
			new GL().renderTexture(this._TextureRight, this._Position.getX(),
				 this._Position.getY() + this._Font.getHeight(), 1);
		}
	}

	this.shoot = function(){
		this._Projectiles.push(new Projectile(this._TextureProjectile,
			this._Position, new Input().getMousePos())); 
	}

	this.create(name, font);

}

//Rhino.js
function Rhino(texture, X,Y, sound){

	this.STEP = 5;
	this._Texture;
	this._Position;
	this._Width = 0;
	this._Height = 0;
	this._Hitbox;
	this._Health = 100;
	this._Invisible;
	this._Hit;

	this.create = function(texture, X, Y, sound){
		this._Hit = sound;
		this._Position = new Vector2(X, Y);
		this._Hitbox = new Hitbox(this._Position.getX(), this._Position.getY(), 192, 96);
		this._Texture = texture;
		this._Width = new Display().getWidth();
		this._Height = new Display().getHeight();
	}

 	this.render = function(){
 		if (this._Health <= 10){
 			this.destroy();
		}
 		if (!(typeof this._Hitbox === "undefined")) {
	 		this._Hitbox = new Hitbox(this._Position.getX(), this._Position.getY(), 192, 96);
	 	}
 		this._Position.setX(this._Position.getX() - this.STEP);
  		if (this._Position.getX() < (0 - 192) ||
 			this._Position.getX() > (this.Width + 192)){
 			this.destroy();
 		}
		if (!this._Invisible){
			new GL().renderTexture(this._Texture, this._Position.getX(), this._Position.getY());
		}
 	}

 	this.destroy = function(){
 		this._Invisible = true;
 		this._Hitbox = undefined;
 	}

	this.create(texture, X,Y, sound);

}


var Instance = Rhino_Instance;
var Arguments = Rhino_Arguments;
function main(){
	inst = new RhinoGame(Instance, Arguments);
	try{
		inst.run();
	}catch (err){
		stackTrace(err);
	}
}

function RhinoGame(Instance, Arguments){

	this._Instance = Instance;
	this._Arguments = Arguments;
	this.camera;
	this._Player;
	this.bunnyDisplayList;
	this._ModelLoader;
	this._Font;

	this.run = function() {

		this._ModelLoader = new ModelLoader();
		this.setUpDisplay();
		this.setUpDisplayLists();
		Utils.log("Model loaded", this);
		var aspect = parseFloat(org.lwjgl.opengl.Display.getWidth() /
					 org.lwjgl.opengl.Display.getHeight());
		var derp = new Builder();
		Utils.log("Camera setup'd", this);
		derp.setAspectRatio(aspect)
		derp.setRotation(-1.12, 0.16, 0)
		derp.setPosition(-1.38, 1.36, 7.95)
		this.camera = derp.build();
		this.camera.applyOptimalStates();
		this.camera.applyPerspectiveMatrix();

		// this._Player = new Player("BeryJu", this._Font);
		// this._Font = new GL().loadFont("ubuntu", 28);
		while (!org.lwjgl.opengl.Display.isCloseRequested()) {
			this.render();
			this.checkInput();
			org.lwjgl.opengl.Display.update();
			org.lwjgl.opengl.Display.sync(60);
		}
		this.cleanUp();
	}

	this.setUpDisplayLists = function(){
		var bunny = this._ModelLoader.loadOBJ("Bullet");
		this.bunnyDisplayList = this._ModelLoader.createDisplayList(bunny);
	}

	this.checkInput = function() {
		this.camera.processMouse(1, 80, -80);
		//delta, speed
		this.camera.processKeyboard(16, 1);
		if (Mouse.isButtonDown(0))
			Mouse.setGrabbed(true);
		else if (Mouse.isButtonDown(1))
			Mouse.setGrabbed(false);
	}

	this.cleanUp = function() {
		GL11.glDeleteLists(this.bunnyDisplayList, 1);
		org.lwjgl.opengl.Display.destroy();
	}

	this.render = function() {
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		/*this._Font.drawString(0,30,this._Instance.getMemoryPercentage().toString()
			+"% Memory used",  Color.white);*/
		GL11.glLoadIdentity();
		this.camera.applyTranslations();
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		// this._Player.render();
		// this._Font.drawString(0,0, "RhinoGame Engine build"+this._Instance.Build);
		GL11.glCallList(this.bunnyDisplayList);
	}

	this.setUpDisplay = function() {
		try {
			org.lwjgl.opengl.Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(1280, 720));
			org.lwjgl.opengl.Display.setVSyncEnabled(true);
			org.lwjgl.opengl.Display.setTitle("RhinoGame");
			org.lwjgl.opengl.Display.create();
		} catch (err) {
			System.err.println("The display wasn't initialized correctly. :(");
			org.lwjgl.opengl.Display.destroy();
			System.exit(1);
		}
	}

}

