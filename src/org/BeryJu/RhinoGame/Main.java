package org.BeryJu.RhinoGame;

import java.io.*;
import org.BeryJu.RhinoGame.JavaScript.*;
import org.BeryJu.RhinoGame.RenderSystem.*;
import org.BeryJu.RhinoGame.Remote.*;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class Main {

	public static long StartTime;
	public SceneManager Scenes = new SceneManager();
	public static boolean debugMode;
	public static boolean disableColors;
	public static boolean dump;
	public static String Build;

	private static final long MEGABYTE = 1024L * 1024L;

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public static long getFreeMemory(){
		return Runtime.getRuntime().freeMemory();
	}

	public static long getTotalMemory(){
		return Runtime.getRuntime().totalMemory();
	}

	public long getMemoryPercentage(){
		long total = Runtime.getRuntime().totalMemory();
		long free = Runtime.getRuntime().freeMemory();
		return free / (total / 100);
	}

	public static void main(String[] args) throws Exception {
		Main.StartTime = System.currentTimeMillis();
		Main m = new Main();
		Main.Build = m.getClass().getPackage().getImplementationVersion();
		Utils.log("RhinoGame build"+Main.Build, null);
		Utils.log("Heap size: "+bytesToMegabytes(Runtime.getRuntime().totalMemory())+"mb", null);
		if (args.length > 0){
			for (String arg : args) {
				if (arg.equals("--debug")){
					Main.debugMode = true;
				}else if (arg.equals("--dump")){
					Main.dump = true;
				}else if (arg.equals("--disable-colors")){
					Main.disableColors = true;
				}else if (arg.equals("--help")){
					System.out.println("RhinoGame "+Main.Build+
						"\t\n--help; This screen\t\n--debug: Engage Debug mode\t\n--dump: "+
						"Dump the PreProcessed Lib\t\n--disable-colors: Disable the colored "+
						"Terminal output");
					System.exit(0);
				}
//				}else if (arg.equals("--client")){
//					Client c = new Client(9898, "localhost");
//				}else if (arg.equals("--server")){
//					Server s = new Server(9898);
//				}
			}
		}
		try {
			File script = new File("game.js");
			ScriptInterface i = new ScriptInterface(script, m, args);
			//.i.collectVariables();
			i.evaluate();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
}