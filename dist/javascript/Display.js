function Display(){

	this.setDisplayMode = function(w, h){
		try{
			org.lwjgl.opengl.Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(w, h));
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.setFullscreen = function(fullscreen){
		try{
			if (fullscreen == true){
				org.lwjgl.opengl.Display.setFullscreen(true);
			}else{
				org.lwjgl.opengl.Display.setFullscreen(false);
			}
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.getWidth = function(){
		try{
			return org.lwjgl.opengl.Display.getDisplayMode().getWidth();
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.getHeight = function(){
		try{
			return org.lwjgl.opengl.Display.getDisplayMode().getHeight();
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.setTitle = function(title){
		try{
			org.lwjgl.opengl.Display.setTitle(title);
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.create = function(){
		try{
			org.lwjgl.opengl.Display.create();
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.requestClose = function(){
		Utils.log("Requested Close", this);
		new AL().destroy();
		this.destroy();
		return java.lang.System.exit(0);
	}

	this.isCloseRequested = function(){
		if (org.lwjgl.opengl.Display.isCloseRequested() == true){
			this.requestClose();
		}
		return false;
	}

	this.destroy = function(){
		org.lwjgl.opengl.Display.destroy();
	}

}