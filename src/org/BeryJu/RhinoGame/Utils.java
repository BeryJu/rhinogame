package org.BeryJu.RhinoGame;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Font;
import java.io.*;
import flexjson.*;
import java.awt.Image;
import java.awt.image.PixelGrabber;
import java.nio.*;
import javax.imageio.ImageIO;
import org.lwjgl.util.Rectangle;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Utils {

	public static String getOS(){
		String base = System.getProperty("os.name");
		return base;
	}

	public static void renderLine(int startx, int starty, int endx, int endy, int r, int g, int b) {
		glDisable(GL_TEXTURE_2D);
		glColor3ub((byte) r, (byte) g, (byte) b);
		glBegin(GL_LINES);
		glVertex2i(startx, starty);
		glVertex2i(endx, endy);
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}

	public static void clearBuffer() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1, 1, 1, 1);
		glLoadIdentity();
	}
	
	public static void setIcon(String path) throws IOException {
		Image img = ImageIO.read(new File(path));
		int len=img.getHeight(null)*img.getWidth(null);
		ByteBuffer temp=ByteBuffer.allocateDirect(len<<2);;
		temp.order(ByteOrder.LITTLE_ENDIAN);

		int[] pixels=new int[len];

		PixelGrabber pg=new PixelGrabber(img, 0, 0, img.getWidth(null), img.getHeight(null), pixels, 0, img.getWidth(null));

		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
		            e.printStackTrace();
		}

		for (int i=0; i<len; i++) {
			int pos=i<<2;
			int texel=pixels[i];
			if (texel!=0) {
				texel|=0xff000000;
			}
			temp.putInt(pos, texel);
		}
		org.lwjgl.opengl.Display.setIcon(new ByteBuffer[]{temp});
	}

	public static void renderColoredQuad(float x, float y, int width, int height, int r, int g, int b) {
		glDisable(GL_TEXTURE_2D);
		glColor3ub((byte) r, (byte) g, (byte) b);
		//glColor3f(multi * (float) r, multi * (float) g, multi * (float) b);
		glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x + width, y);
		glVertex2f(x + width, y + height);
		glVertex2f(x, y + height);
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}

	public static String serialize(Object o){
		return new flexjson.JSONSerializer().serialize(o);
	}

	public static Object deserialize(String s){
		return new JSONDeserializer<Object>().deserialize(s);
	}

	public static void renderColoredQuad(int x, int y, int width, int height, float r, float g, float b, float a) {
		glDisable(GL_TEXTURE_2D);
		glColor4f(r, g, b, a);
		glBegin(GL_QUADS);
		glVertex2i(x, y);
		glVertex2i(x + width, y);
		glVertex2i(x + width, y + height);
		glVertex2i(x, y + height);
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}

	private static void drawLinePartial2x(int Xa, int Ya, int Xb, int Yb) {
		glBegin(GL_LINES);
		glVertex2i(Xa, Ya);
		glVertex2i(Xb, Yb);
		glVertex2i(Xa + 1, Ya + 1);
		glVertex2i(Xb + 1, Yb + 1);
		glVertex2i(Xa - 1, Ya - 1);
		glVertex2i(Xb - 1, Yb - 1);
		glEnd();
	}

	public static void drawRectangle(Rectangle re, float r, float g, float b, float a) {
		glDisable(GL_TEXTURE_2D);
		glColor4f(r, g, b, a);
		int tx = re.getX() - 4;
		int ty = re.getY() + 2;
		int th = re.getHeight() + 2;
		int tw = re.getWidth() + 4;
		//top
		drawLinePartial2x(tx, ty, tx + tw, ty);
		//bottom
		drawLinePartial2x(tx, ty + th, tx + tw, ty + th);
		//left
		drawLinePartial2x(tx, ty, tx, ty + th);
		//right
		drawLinePartial2x(tx + tw, ty, tx + tw, ty + th);
		glEnable(GL_TEXTURE_2D);
	}

	public static void log(Object content, Object sender) {
		String blue = "";
		String green = "";
		String red = "";
		String yellow = "";
		String reset = "";
		if (!Main.disableColors){
			blue = "\033[1;34m";
			green = "\033[1;32m";
			red = "\033[31m";
			yellow = "\033[1;33m";
			reset = "\033[0m";
		}
		if (sender != null) {
			if (!sender.getClass().getSuperclass().getSimpleName().equals("Object")) {
				System.out.println(yellow+"[" + toLength(String.valueOf(System.currentTimeMillis() - Main.StartTime), 6) + "]"
						+ blue+"[" + sender.getClass().getSimpleName()
						+ green + " (extends " + sender.getClass().getSuperclass().getSimpleName()+ ")"+ blue+"]"
						+ red+content.toString() + reset);
			} else {
				System.out.println(yellow+"[" + toLength(String.valueOf(System.currentTimeMillis() - Main.StartTime), 6) + "]"
						+ blue+"[" + sender.getClass().getSimpleName() + "]"
						+ red+content.toString()+reset);
			}
		} else {
			System.out.println(yellow+"[" + toLength(String.valueOf(System.currentTimeMillis() - Main.StartTime), 6) + "]" +red+ content.toString() + reset);
		}
	}

	private static String repeat(String s, int amount){
		String ret = "";
		for (int i = 0; i == amount; i++){
			ret += s;
		}
		return ret;
	}

	public static String toLength(String s, int l){
		if (s.length() > l){
			return s.substring(0, l);
		}else if (s.length() < l){
			return s + repeat(" ",s.length() - l);
		}else{
			return s;
		}
	}

	public static void logHTML(Object content, Object sender) {
		if (sender != null) {
			if (!sender.getClass().getSuperclass().getSimpleName().equals("Object")) {
				System.out.println("<div class=\"log\"><p><strong class=\"sender\">"
						+ sender.getClass().getSimpleName() + "</strong>"
						+ "<small class=\"super\"> (extends "
						+ sender.getClass().getSuperclass().getSimpleName()
						+ ")</small> @ "
						+ String.valueOf(System.currentTimeMillis() - Main.StartTime)
						+ "</p><span class=\"content\">"
						+ content.toString()
						+ "</span></div>");
			} else {
				System.out.println("<div class=\"log\"><p><strong class=\"sender\">"
						+ sender.getClass().getSimpleName() + "</strong> @ "
						+ String.valueOf(System.currentTimeMillis() - Main.StartTime)
						+ "</p><span class=\"content\">"
						+ content.toString()
						+ "</span></div>");
			}
		} else {
			System.out.println("<div class=\"log\"><p class=\"time\"> @ "
					+ String.valueOf(System.currentTimeMillis() - Main.StartTime)
					+ "</p><span class=\"content\">" + content.toString() + "</span></div>");
		}
	}
}