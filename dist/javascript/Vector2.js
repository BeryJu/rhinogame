function Vector2(X, Y){

	/**
	 * X
	 * @type {number}
	 */
	this.X = X;
	/**
	 * Y
	 * @type {number}
	 */
	this.Y = Y;

	/**
	 * Get X
	 * @return {number} X of this
	 */
	this.getX = function(){
		return this.X;
	}

	/**
	 * Get Y
	 * @return {number} Y of this
	 */
	this.getY = function(){
		return this.Y;
	}

	/**
	 * Set X
	 * @param {number} newX new value X
	 */
	this.setX = function(newX){
		this.X = newX;
	}

	/**
	 * Set Y
	 * @param {number} newY new value Y
	 */
	this.setY = function(newY){
		this.Y = newY;
	}


	this.toString = function(){
		return "X: "+this.X.toString()+", Y: "+this.Y.toString();
	}

	this.equals = function(a){
		var q = a.getX() == this.getX();
		var w = a.getY() == this.getY();
		return q && w;
	}

}
VectorLength = function(v){
	return Math.sqrt(v.X*v.X+v.Y*v.Y)
}

VectorNormalized = function(v){
	var l = VectorLength(v)
	return new Vector2(v.X/l, v.Y/l);
}

VectorSub = function(v1, v2){
	return new Vector2(v1.X-v2.X, v1.Y-v2.Y);
}

VectorAdd = function(v1, v2){
	return new Vector2(v1.X+v2.X, v1.Y+v2.Y);
}

VectorMul = function(v1, v2){
	return new Vector2(v1.X*v2.X, v1.Y*v2.Y);
}

VectorInterpolate = function(obj, dest, speed){
	var dir = VectorNormalized(VectorSub(dest, obj))
	var dir2 = VectorAdd(obj, VectorMul(dir, speed));
	return VectorRound(dir2);
}

VectorRound = function(v){
	return new Vector2(Math.round(v.getX()),Math.round(v.getY()));
}