importPackage(java.lang);
function printArray(array){
	if (array instanceof Array){
		foreach(array, function(e){
			if (e instanceof Array){
				printArray(e);
			}else{
				System.out.println(e.toString());
			}
		});
	}else{
		return false;
	}
}

function stackTrace(e){
	Utils.log(e.name, this);
	if(e.rhinoException != null){
		e.rhinoException.printStackTrace();
	}else if(e.javaException != null){
		e.javaException.printStackTrace();
	}
}