package org.BeryJu.RhinoGame.JavaScript;

import java.util.*;
import java.io.*;
import org.BeryJu.RhinoGame.*;

public class PreProcessor{

	public class Annotation{

		public String removeChar(String in, int l){
			return in.substring(0, in.length() - l);
		}

		private int lineCount = 0;

		public String line(String in, LibLoader inst){
			String line = in.trim();
			lineCount++;
			if (line.startsWith("@")){
				String action = line.substring(1);
				if (action.startsWith("load")){
					File f = new File(removeChar(action.split("<")[1], 1));
					if (f.isDirectory()){
						String files = "";
						for (File sub : f.listFiles()){
							Utils.log("Loaded file "+f.getName()+"/"
								+sub.getName(), inst);
							files += "//"+sub.getName() + "\n"+
								readFile(sub, inst)+"\n";
						}
						return files;
					}else{
						Utils.log("Loaded file "+f.getName(), inst);
						return readFile(f, inst);
					}
				}else if (action.startsWith("debug")){
					String mod = removeChar(action.split("<")[1], 1);
					if (mod.equalsIgnoreCase("on")){
						Main.debugMode = true;
					}else{
						Main.debugMode = false;
					}
				}
			}else if (line.startsWith("foreach(")){
				try{
					String sig = removeChar(line.replace("foreach(", ""), 2);
					String[] arg = sig.split(":");
					return "for (var i = 0; i < "+arg[0]+".length; i++) {\n"+
							"var "+arg[1]+" = "+arg[0]+"[i];";
				}catch (ArrayIndexOutOfBoundsException e){
					Utils.log("Error in script evaluation at line "+lineCount, this);
				}
			}else if (line.startsWith("private")){
				return line.replace("private ", "var ");
			}else if (line.startsWith("public")){
				try{
					String name = line.split(" ")[1].split("[(]")[0];
					String sig = line.split(" ")[1].split("[(]")[1];
					return "this."+name+" = function("+sig;
				}catch (Exception e){
					return line.replace("public ", "this.");
				}
			}else if (line.startsWith("class ")){
				String name = removeChar(line.split(" ")[1], 1);
				return "function "+name+"(){";
			}else{
				//Otherwise there would be no code
				return in;
			}
			return "";
		}

		public String readFile(File f, LibLoader inst){
			BufferedReader br = null;
			String content = "";
			Annotation a = new Annotation();
			try {
				String line;
				br = new BufferedReader(new FileReader(f));
				while ((line = br.readLine()) != null) {
					if (line.startsWith("//") == false){
						if (line.startsWith("@")){
							content += a.line(line, inst);
						}else{
							content += line+"\n";
						}
					}
				}
				return content;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null){
						br.close();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
			return null;
		}

	}

	public class LibLoader{

		private ArrayList<File> libs = new ArrayList<File>();

		public String readFile(File f){
			BufferedReader br = null;
			String content = "";
			Annotation a = new Annotation();
			try {
				String line;
				br = new BufferedReader(new FileReader(f));
				while ((line = br.readLine()) != null) {
					if (line.startsWith("//") == false){
						content += a.line(line, this)+"\n";
					}
				}
				return content;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null){
						br.close();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
			return null;
		}

		public ArrayList<File> getLibs(){
			return this.libs;
		}

		public LibLoader(String dir){
			load(new File(dir));
		}

		public String append(){
			String libs = "";
			for (File f : this.libs) {
				libs += "//"+f.getName()+"\n"+readFile(f);
			}
			return libs;
		}

		public void load(File dir){
			for (File f : dir.listFiles()) {
				if (f.isDirectory()){
					load(f);
				}else{
					if (f.getName().endsWith(".js")){
						this.libs.add(f);
					}
				}
			}
		}

	}



}