/**
 * @author BeryJu, ported from CodingUniverse
 */
importPackage(java.uitl);
importPackage(org.lwjgl.util.vector);
function Model(){

	this.vertices = [];
	this.normals = [];

	this.faces = [];

}

function Face(vertex, normal){

	this.vertex = vertex;
	this.normal = normal;

}