package org.BeryJu.RhinoGame;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class FPSCounter {

	private long lastFrame;
	private int fps;
	private long lastFPS;
	private int actualFPS;

	public FPSCounter() {
		getDelta();
		lastFPS = getTime();
	}
	
	private int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;
		return delta;
	}

	private long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public int getFPS(){
		return this.actualFPS;
	}

	public void update() {
		getDelta();
		if (getTime() - lastFPS > 1000) {
			this.actualFPS = fps;
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}
}