package org.BeryJu.RhinoGame.RenderSystem;

import org.BeryJu.RhinoGame.Main;

public abstract class EntityHandler {

	public abstract void init(Entity sender, Main instance);
	public abstract void click(Entity sender, Main instance);
	public abstract void rightClick(Entity sender, Main instance);
	
}